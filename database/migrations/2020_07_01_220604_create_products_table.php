<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('price')->nullable();
            $table->float('purchase_price')->nullable();
            $table->string('comercial_house')->nullable();
            $table->float('offer_price')->nullable();
            $table->longText('description_large')->nullable();
            $table->text('description')->nullable();

            $table->string('code')->nullable();
            $table->string('stock')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('avatar')->nullable();
            $table->tinyInteger('destake')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::table('products', function($table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

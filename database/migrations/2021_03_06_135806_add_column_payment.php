<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            
               //mp
               $table->bigInteger('collection_id')->nullable();
               $table->string('collection_status')->nullable();
               $table->string('external_reference')->nullable();
               $table->string('payment_type')->nullable();
               $table->string('payment_id')->nullable();
               $table->bigInteger('merchant_order_id')->nullable();
               $table->text('preference_id')->nullable();
               $table->string('processing_mode')->nullable();
               $table->string('merchant_account_id')->nullable();
            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

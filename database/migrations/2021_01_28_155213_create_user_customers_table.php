<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_customers', function (Blueprint $table) {
            $table->id(); 
            $table->string('distric')->nullable();
            $table->string('street')->nullable();
            $table->string('number')->nullable();
            $table->string('dni')->nullable();
            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->text('addresse2')->nullable();
            $table->text('addresse')->nullable();
            $table->string('codePostal')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('phone')->nullable();
            $table->text('token');

            $table->timestamps();
            $table->foreignId('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_customers');
    }
}

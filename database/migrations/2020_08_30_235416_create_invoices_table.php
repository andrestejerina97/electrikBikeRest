<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Invoices', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->text('observation')->nullable();
            $table->string('cellphone')->nullable();
            $table->double('home')->nullable();
            $table->string('pay')->nullable();
            $table->double('total')->nullable();
            $table->double('discount')->nullable();
            $table->double('total_delivery')->nullable();
            $table->integer('status')->nullable();
            $table->unsignedBigInteger('modified_id');
            $table->unsignedBigInteger('customer_id')->nullable();
           // $table->unsignedBigInteger('seller_id')->nullable();

            $table->unsignedBigInteger('creatur_id');
         //   $table->foreign('seller_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('modified_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('creatur_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}

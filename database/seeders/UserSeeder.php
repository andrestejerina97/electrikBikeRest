<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::insert([
            'name'=>'Electrik'
        ]);
      
        Category::insert([
            'category_name'=>'Bicicletas electricas',
            'customer_id'=>1
        ]);
        Category::insert([
            'category_name'=>'Bicicletas electricas de niños',
            'customer_id'=>1
        ]);
    }
}

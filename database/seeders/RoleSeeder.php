<?php
namespace Database\Seeders;

use App\Models\Customer;
use App\Models\status;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           $customer= Customer::create([
            'name' => "administrador",
            'is_active'=>1,
            ]);

            Role::create(['name' => 'customer']);
            Role::create(['name' => 'admin']);
            Role::create(['name' => 'user']);
            $permission = Permission::create(['name' => 'Editar']);
            $permission = Permission::create(['name' => 'Admin']);

            $user=User::create([
                'name' => "administrador",
                'email' => "administrador@gmail.com",
                'password' => Hash::make("administrador1234"),
                'customer_id'=> 1,
            ]);
            $user->assignRole("admin");
            $user=User::create([
                'name' => "user",
                'email' => "user@gmail.com",
                'password' => Hash::make("user1234"),
                'customer_id'=> 1,

            ]);
            $user->assignRole("user");
            $user=User::create([
                'name' => "customer",
                'email' => "customer@gmail.com",
                'password' => Hash::make("customer1234"),
                'customer_id'=> 1,

            ]);
            $user->assignRole("customer");

            status::create([
                'name'=>'Pagado',
                'customer_id'=> 1,

            ]);
            status::create([
                'name'=>'Pendiente',
                'customer_id'=> 1,

            ]);
            status::create([
                'name'=>'Anulado',
                'customer_id'=> 1,

            ]);
    }
}

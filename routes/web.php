<?php

use App\Http\Controllers\Admin\CarouselController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ProductController;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\WebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|->middleware('auth')
define('STDIN',fopen("php://stdin","r"));
Route::get('install', function() {
    Artisan::call('migrate',['--force'=>true,'--seed'=>true]);
});
*/

Auth::routes();

Route::namespace('admin')->middleware('auth')->group(function(){
    Route::get('/', [HomeController::class, 'index'] )->name('home');
    Route::get('/root/admin', [HomeController::class, 'index'] )->name('root');
    Route::post('/root/admin-guardar', [HomeController::class, 'saveIndex'] )->name('root.store');
    Route::post('/root/admin-editar', [HomeController::class, 'updateIndex'])->name('root.update');

    Route::get('/root/editar/{user}',[HomeController::class, 'editIndex'])->name('root.edit');

    //Productos
    Route::get('/productos',[ProductController::class, 'index'])->name('admin.index.product');
    Route::get('/mi-producto/{id?}',[ProductController::class, 'edit'])->name('admin.edit.product');
    Route::post('/guardar-producto/{id?}',[ProductController::class, 'store'])->name('admin.store.product');
    Route::post('/actualizar-producto/{id?}',[ProductController::class, 'update'])->name('admin.update.product');
    Route::get('/borrar-producto/{id?}',[ProductController::class, 'delete'])->name('admin.delete.product');
    Route::get('/producto-filtro',[ProductController::class, 'search'])->name('admin.search.product');
    Route::get('/actualizar-foto-de-perfil/{id?}',[ProductController::class, 'activePhoto'])->name('admin.active.photo');
    Route::get('/eliminar-foto-de-perfil/{id}',[ProductController::class, 'deletePhoto'])->name('admin.delete.photo');
    //Categorías
    Route::get('/categorías',[CategoriesController::class, 'index'])->name('admin.index.categories');
    Route::get('/mi-categoría/{id?}',[CategoriesController::class, 'edit'])->name('admin.edit.category');
    Route::post('/guardar-categoría/{id?}',[CategoriesController::class, 'store'])->name('admin.store.category');
    Route::post('/actualizar-categoría/{id?}',[CategoriesController::class, 'update'])->name('admin.update.category');
    Route::get('/borrar-categoría/{id?}',[CategoriesController::class, 'delete'])->name('admin.delete.category');
    Route::get('/categoría-filtro',[CategoriesController::class, 'search'])->name('admin.search.category');


    //empleados
    Route::get('/empleados',[EmployeeController::class, 'index'])->name('admin.index.employee');
    Route::get('/mi-empleado/{id?}','EmployeeController@edit')->name('admin.edit.employee');
    Route::post('/guardar-empleado/{id?}','EmployeeController@store')->name('admin.store.employee');
   // Route::post('/actualizar-empleado/{id?}','EmployeeController@update')->name('admin.update.employee');
    Route::get('/eliminar-empleado/{id?}','EmployeeController@delete')->name('admin.delete.employee');
    Route::get('/empleado-filtro','EmployeeController@search')->name('admin.search.employee');
    
    //reservaciones
    Route::get('/turnos','ReservationController@index')->name('admin.index.reservation');
    Route::get('/mi-turno-actual/{id?}/{date?}','ReservationController@edit')->name('admin.edit.reservation');
    Route::post('/guardar-turno-/{id?}','ReservationController@store')->name('admin.store.reservation');
    Route::post('/actualizar-turno/{id?}','ReservationController@update')->name('admin.update.reservation');
    Route::get('/eliminar-turno/{id?}','ReservationController@delete')->name('admin.delete.reservation');
    Route::post('/turno-filtro','ReservationController@search')->name('admin.search.reservation');
    Route::get('/filtro-turno','ReservationController@search')->name('admin.search.reservation');

    //Servicios
    Route::get('/servicio','ServiceController@index')->name('admin.index.service');
    Route::get('/mi-servicio-/{id?}','ServiceController@edit')->name('admin.edit.service');
    Route::post('/guardar-servicio/{id?}','ServiceController@store')->name('admin.store.service');
    Route::get('/eliminar-servicio/{id?}','ServiceController@delete')->name('admin.delete.service');
    Route::get('/servicio-filtro','ServiceController@search')->name('admin.search.service');
    
    //usuarios
    Route::get('/usuarios',[UserController::class, 'index'])->name('admin.index.user');
    Route::get('/mi-usuario/{id?}',[UserController::class, 'edit'])->name('admin.edit.user');
    Route::post('/guardar-usuario-/{id?}',[UserController::class, 'store'])->name('admin.store.user');
    Route::get('/eliminar-usuario/{id?}',[UserController::class, 'delete'])->name('admin.delete.user');
    Route::get('/servicio-usuario',[UserController::class, 'search'])->name('admin.search.user');
    Route::get('/crear', 'HomeController@crear')->name('craea');

    //Carrusel
    Route::get('/carruseles',[CarouselController::class, 'index'])->name('admin.index.carousel');
    Route::get('/mi-carrusel/{id?}',[CarouselController::class, 'edit'])->name('admin.edit.carousel');
    Route::post('/guardar-carrusel-/{id?}',[CarouselController::class, 'store'])->name('admin.store.carousel');
    Route::get('/eliminar-carrusel/{id?}',[CarouselController::class, 'delete'])->name('admin.delete.carousel');
    Route::get('/servicio-carrusel',[CarouselController::class, 'search'])->name('admin.search.carousel');
    Route::get('/crear', 'HomeController@crear')->name('crear');

    //Configs
    Route::get('/configuraciones',[ConfigController::class, 'index'])->name('admin.index.config');
    Route::get('/mi-cofig/{id?}',[ConfigController::class, 'edit'])->name('admin.edit.config');
    Route::post('/guardar-config-/{id?}',[ConfigController::class, 'store'])->name('admin.store.config');
    Route::get('/eliminar-config/{id?}',[ConfigController::class, 'delete'])->name('admin.delete.config');
    Route::get('/servicio-config',[ConfigController::class, 'search'])->name('admin.search.config');
    Route::get('/crear', 'HomeController@crear')->name('crear');
});
Route::get('cancel/{pay}/{invoice}',[PaymentController::class, 'cancelPayment'])->name('payment.fails');

Route::get('cancel/{pay}/{invoice}',[PaymentController::class, 'cancelPayment'])->name('payment.cancel');
Route::get('payment/success/{pay}/{invoice}', [PaymentController::class, 'successPayment'])->name('payment.success');
Route::get('/compra-exitosa',function(){
    return view('plans.buy-success');
})->name('buy.success');

<?php

use App\Http\Controllers\CarouselController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserCustomerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/products', [ProductController::class, 'index']);
Route::get('/products/{id}', [ProductController::class, 'show']);
Route::get('/productsDestake', [ProductController::class, 'destake']);
Route::get('/productsMoreSale',[ProductController::class, 'getMoreSaled'])->name('MoreSaled');
Route::post('/payment/generate',[PaymentController::class, 'getLinkToPay'])->name('payment.generate');
Route::post('/payment/create',[PaymentController::class, 'processPayment'])->name('payment.create');

/**
 * Configs
 */
Route::get('/configs/sendTax', [ConfigController::class, 'getSendTax']);

/**
 * Categories
 */
Route::get('/categories', [CategoriesController::class, 'index']);
Route::get('/categories/{id}', [CategoriesController::class, 'show']);

/**
 * Carousels
 */
Route::get('/carousel', [CarouselController::class, 'index']);
//Route::get('/carousel/{id}', [CategoriesController::class, 'show']);


/**
 * Buy
 */
Route::get('/buy/{token}',[WebController::class, 'index'])->name('homeVue');

/**
 * UsersCustomers
 */
Route::get('/userCustomers/getUser',[UserCustomerController::class, 'index'])->name('homeVue');
Route::post('/userCustomers/getUser',[UserCustomerController::class, 'create'])->name('homeVue');

Route::post('/provinces/getProvince',[UserCustomerController::class, 'create'])->name('homeVue');
Route::group(['middleware' => ['cors']], function () {

Route::post('/emails/sendEmailWholesale',[UserCustomerController::class, 'sendEmailWholesale'])->name('sendEmailWholesale');
Route::post('/emails/contact',[UserCustomerController::class, 'sendContact'])->name('sendContact');

});
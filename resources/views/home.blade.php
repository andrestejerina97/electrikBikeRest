@extends('layouts.admin')
@section('content')
<div class="row">
   <div class="col-md-12">
    <div class="col-lg-4">
        <a href="{{route('admin.index.product')}}" style="color:#fff">
        <div class="card bg-primary">
             <div class="card-content">
                <h4 class="title" id="encabezados">
                    <i class="fa fa-th-list"></i>

                    Productos
                </h4>
             </div>
            </a>
        </div>
        
     </div>
     <div class="col-lg-4">
        <a href="{{route('admin.index.categories')}}" style="color:#fff">
         <div class="card bg-secondary">
            <div class="card-content">
                <h4 class="title" id="encabezados">
                    <i class="fa fa-support"></i>

                   Categorías
                </h4>
             </div>
            
            </div></a>
     </div>
     <div class="col-lg-4">
        <a href="{{route('admin.index.user')}}" style="color:#fff">
         <div class="card bg-3 ">
            <div class="card-content">
                <h4 class="title" id="encabezados">
                    <i class="fa fa-users"></i>

                    Usuarios
                </h4>
             </div>
            
            </div></a>
     </div>
 
   </div>
</div>
@endsection


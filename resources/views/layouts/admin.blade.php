<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>{{Session::get('customerName')}}</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="viewport" content="width=device-width" />
	  <meta name="description" content="Capillum" /> 
    <meta name="author" content="Capillum-LiderIt-QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Tienda">
    <meta name="theme-color" content="#ffffff"/>
    <meta name="description" content="TiendaGest la mejor forma de administrar tu negocio">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('img/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon-16x16.png')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("app/icons/icon-152x152.png")}}">
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/material-dashboard.css?v3')}}" rel="stylesheet"/>
<link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
<link rel="stylesheet" href="{{asset('customcss/estilistas.css?v=2')}}">

<style>
  #top {
    color:black; 
    font-family: elsie; 
    background-color:#316ceb;
}

#fondo{ 
    background-color: #FFFFFF;
}

#encabezados{
    color:rgb(255, 255, 255); 
    font-family: elsie;
    text-align: center;
}
#collapse:hover {
    background-color:#2950a3;
}


  .principal{
    background-color:#629fee !important;
    color: #ffffff !important;

}

.loader {
  position: fixed;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  width: 100vw;
  height: 100vh;
  z-index: 99999;
  background-color: #ffffff;
  opacity: 0.7;

}
		.whatsapp {
		  position:fixed;
		  width:50px;
		  height:50px;
		  bottom:30px;
		  right:30px;
		  background-color:#25d366;
		  color:#FFF;
		  border-radius:40px;
		  text-align:center;
		  font-size:25px;
		  z-index:100;
		}

		.whatsapp-icon {
		  margin-top:13px;
		}
</style>
@yield('css')
</head>

<body>
  <div id="loader" hidden class="loader">
    <div class="text-center" style="position: relative !important; top: 25vh; ">
      <a href="#" class="text-center"><img src="{{asset(Auth::user()->customers->logo)}}"  style="width: 40%; border-radius: 80px"></a>
      <h2 style="color:#000000">Cargando....</h2>

    </div>  
  </div>
  <div>
    <a href="https://wa.me/543516687683?text=Hola,%20tengo%20una%20consulta%20sobre%20Mi%20tienda" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>
        </div>
  <div class="wrapper">
    
      @include('layouts.sidebar.admin')
      <div class="main-panel" id="top">
      <nav class="navbar navbar-transparent navbar-absolute ">
        
        <div class="container-fluid">
          <div class="navbar-header">
   
            <div class="row">
              <button type="button" class="navbar-toggle" style="color:  #ffffff !important;" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                 
                <div style="position: fixed; left: 10%; padding:0px; margin:0px">
                <h3  style="color: #ffffff; "><i class="fa fa-user" aria-hidden="true" ></i>
                 @isset(Auth::user()->customers->name)
                 {{Auth::user()->customers->name}}
                 @endisset
                </h3>
                </div>
            </div>


          </div>
          <div class="collapse navbar-collapse" id="collapse">
          
          </div>
        </div>
      </nav>
    
      <div class="content" id="fondo">
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
    </div>

  </div>
</body>
<script src="{{asset('js/pwabuilder-sw.js')}}"></script>
<script  type="module"  src="{{asset('js/pwabuilder-sw-register.js')}}"></script>

  <!--   Core JS Files   -->
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/material.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/material-dashboard.js')}}"></script>

  <!--  Charts Plugin -->
<script src="{{asset('js/chartist.min.js')}}"></script>

  <!--  Notifications Plugin    -->
<script src="{{asset('js/bootstrap-notify.js')}}"></script>


  <!-- Material Dashboard javascript methods -->

  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script type="text/javascript">
        $("#loader").hide();

      $(document).ready(function(){

    

 
      });

      $(window).on("beforeunload", function() { 
  $("#loader").show();
});
    

  </script>

  @yield('scripts')

  

</html>

@component('mail::message')
{{$message}}

@component('mail::table')
| Resumen       | Datos         |
| ------------- |:-------------:|
| Nombre      | {{$data['name']}}      |
| Email       | {{$data['email']}} |
| Empresa/Razón social     | {{$data['bussiness']}} |
| Teléfono     | {{$data['phone']}} |
| CUIT     | {{$data['cuit']}} |
| Forma de pago     | {{$data['payType']}} |
| Tipo de producto     | {{$data['productType']}} |
| Mensaje     | {{$data['message']}}|

@endcomponent

Comunícate a la brevedad.
@endcomponent

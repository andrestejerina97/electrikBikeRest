@component('mail::message')
    {{ $message }}

@component('mail::table')
| Resumen       | Datos         |
| ------------- |:-------------:|
| Bolsa     |{{ $data['bag_type'] }} |
| Impresión | {{ $data['handles_type'] }}|
| Tamaño    | {{ $data['size'] }} |
| Mensaje   | {{ $data['message'] }} |
| Manija    |{{ $data['print_type'] }} |
| Email de contacto | {{ $data['email'] }} |
@endcomponent

Comunícate a la brevedad.
@endcomponent

@extends('layouts.admin')
@section('css')
<link href="{{asset('css/owl.carousel.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                @isset($categories)
                <h4 class="title">Editar Categoría</h4>
                @else
                <h4 class="title">Nuevo Categoría</h4>

                @endisset
            </div>
            <div class="card-content table-responsive">
                <form class="form-horizontal"  accept-charset="UTF-8"
                enctype="multipart/form-data" method="post" onsubmit="save(event)"  id="form_category" action="{{route('admin.update.category')}}"
                    role="form">
                    @csrf
                    @isset($categories)
                    @foreach ($categories as $category)
            <input type="hidden" name="id" value="{{$category->id}}">
                    @include("admin.partials.form-categories")
    
                    @endforeach
                    @else
                 @include("admin.partials.form-categories")

                @endisset
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                         
                        @isset($category)
                        <button type="submit" class="btn btn-primary">Actualizar Categoría</button>
                        @else
                        <button type="submit" class="btn btn-primary">Guardar Categoría</button>
                    
                        @endisset
                        </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

 
    function save(e) {
        e.preventDefault();
        var formdata = new FormData($("#form_category")[0]);
            $.ajax({
                url         : "{{route('admin.store.category')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Guardado con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                       // let url="{{route('admin.edit.category',['id'=>0])}}"
                        //url=url.replace('0',data.result);
                        location.href='{!! url()->previous()!!}';//url;
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {

                      console.log(data.responseJSON.errors);
                      let lista= "";
                for(var k in data.responseJSON.errors) {
                lista += ""+ data.responseJSON.errors[k][0] +"";              
                }
                
              lista+="";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error: '+lista,
                      icon: 'error',
                    });                    }
                  }
            });
    }
</script>
@endsection
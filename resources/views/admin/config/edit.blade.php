@extends('layouts.admin')
@section('css')
<link href="{{asset('css/owl.config.min.css')}}" rel="stylesheet"/>
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

@endsection
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                @isset($configs)
                <h4 class="title">Editar Item </h4>
                @else
                <h4 class="title">Nuevo Item </h4>

                @endisset
            </div>
            <div class="card-content table-responsive">
                <form class="form-horizontal"  accept-charset="UTF-8"
                enctype="multipart/form-data" method="post" onsubmit="save(event)"  id="form_config" action="{{route('admin.store.config')}}"
                    role="form">
                    @csrf
                    @isset($configs)
                    @foreach ($configs as $config)
                <input type="hidden" name="id" value="{{$config->id}}">
                    @include("admin.partials.form-config")
    
                    @endforeach
                    @else
                 @include("admin.partials.form-config")

                @endisset
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-8 text-center ">
                         
                        @isset($config)
                        <button type="submit" class="btn btn-primary">Actualizar item</button>
                        @else
                        <button type="submit" class="btn btn-primary">Guardar item</button>
                    
                        @endisset
                        </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/owl.config.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/es.js')}}" type="text/javascript"></script>
<script>
      var active='input';
  /***********************Events******************************/
  $(document).on('click','#btn_modal_delete',function(){
    delete_photo(this);
  });
  $(document).on('click','#btn_modal_select_profile',function(){
    selected_photo_for_profile(this);
  });
  $(document).on('click','.configPhoto',function(){
    click_photo(this);
  });
  $(document).on('change','#type',function(){
    changeType()
  });
  /*********************************************************/
      
      /****************Initialite components******************/
      if ($(".configPhotos").length > 0) {
        $(".configPhotos").owlconfig({
         loop:false,
          items: 1,
          margin:10,
          center: true,
          nav:true,
          autoplay:true,
          responsiveClass:true,
          }); 

          $('.select2').select2({
          language: "es",
          tags: true,

        });
      }
      changeType();
    /**********************************/

    function changeType() {
     
      if (
        $('#type').val()=='SHIPPING_POLICY' ||
        $('#type').val()=='CONDITIONS'  ||
        $('#type').val()=='MSG_PAY_EF'  ||
        $('#type').val()=='MSG_PAY_TB'  ||
        $('#type').val()=='MSG_COMPANY'  ||
        $('#type').val()=='PRIVACITY_POLICY'  

      ) {
        $("#value").css('display','none');
        $("#textValue").css('display','block');
        $(".radioValue").css('display','none');

        active='text';
      }else if( 
        $('#type').val()=='PICKUPPOINT_DELIVERY' ||
        $('#type').val()=='METHOD_PAY_MP' ||
        $('#type').val()=='METHOD_PAY_EF' ||
        $('#type').val()=='METHOD_PAY_TB' ||
        $('#type').val()=='PICKUPPOINT_LOCAL'  
        ){
        $("#value").css('display','none');
        $("#textValue").css('display','none');
        $(".radioValue").css('display','block');
        active='radio';
        }
      else{
        $("#value").css('display','block');
        $("#textValue").css('display','none');
        $(".radioValue").css('display','none');
        active='input';

      }
      $("#inputTitle").val($('#type option:selected').text());

    }
    function save(e) {
      e.preventDefault();

      if (active=='input') {
        $("#textValue").text($("#value").val()) 
      }else if(active=='radio'){
        $("#textValue").text($("input[name=radioValue]:checked").val()) 
      }
        var formdata = new FormData($("#form_config")[0]);
            $.ajax({
                url         : "{{route('admin.store.config')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"item guardado con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                       // let url="{{route('admin.edit.config',['id'=>0])}}"
                        //url=url.replace('0',data.result);
                        location.href='{!! url()->previous()!!}';//url;
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {

                      console.log(data.responseJSON.errors);
                      let lista= "";
                for(var k in data.responseJSON.errors) {
                lista += ""+ data.responseJSON.errors[k][0] +"";              
                }
                
              lista+="";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error: '+lista,
                      icon: 'error',
                    });                    }
                  }
            });
    }

    function click_photo(a) {
        let photo=$(a).data("photo");
        let photo_id=$(a).data("photo_id");
        let id=$(a).data("id");

        $("#btn_modal_select_profile").data("photo",photo);
        $("#btn_modal_select_profile").data("photo_id",photo_id);
        $("#btn_modal_delete").data("photo",photo);
        $("#btn_modal_delete").data("id",id);
        $("#btn_modal_delete").data("photo_id",photo_id);
        $("#modal_photo").modal("show");
      }
      function selected_photo_for_profile(a) {
        let photo=$(a).data("photo");
        let photo_id=$(a).data("photo_id");
         
          let url="{{route('admin.active.photo','id')}}"
          url=url.replace("id",photo_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"photo="+photo,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Foto de perfil cargada con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }
                  }
            });
      
      }

      function delete_photo(a) {
        let photo=$(a).data("photo");
        let photo_id=$(a).data("photo_id");
        let id=$(a).data("id");
         $(a).attr("disable",true);
        let url="{{route('admin.delete.photo','id')}}"
          url=url.replace("id",photo_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"photo="+photo+"&id_photo="+id,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result==1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Foto eliminada con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }else{
                      swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");

                    }
                    $(a).attr("disable",false);

                  }
            });
      }

      function selected_photo_for_profile(a) {
        let photo=$(a).data("photo");
        let photo_id=$(a).data("photo_id");
         
          let url="{{route('admin.active.photo','id')}}"
          url=url.replace("id",photo_id);
          $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                data:"photo="+photo,
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Foto de perfil cargada con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.reload();
                      });
                      
                    }
                  }
            });
      
      }
</script>
@endsection
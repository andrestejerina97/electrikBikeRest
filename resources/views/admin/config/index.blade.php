@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
<!--<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-download"></i> Descargar <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
  </ul>
</div>
-->
</div>
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">ITEMs</h4>
  </div>
  <div class="card-content table-responsive">
	
    <form method="get" action="{{route('admin.search.config')}}" role="form"  class="form-horizontal">
      <input type="hidden" name="view" value="configs">
		<div class="form-group">
			<div class="col-lg-3">
				<input type="text" name="title" placeholder="Nombre" class="form-control">
			</div>
			<div class="col-lg-3">
        	</div>
			<div class="col-lg-1">
				
			</div>
			<div class="col-lg-2">
				<button class="btn" >Buscar</button>
			</div>
			<div class="col-lg-2">
            <a href="{{route('admin.edit.config')}}" class="btn btn-primary"><i class='fa fa-plus'></i> Nuevo ITEM</a>
			</div>
			
		</div>
	</form>
			
			<table class="table table-bordered table-hover">
			<thead>
			<th>Título</th>
			<th>Valor</th>
			<th></th>
            </thead>
            @if (count($configs)>0)
                
			@foreach($configs as $config)
				
				<tr>
                <td>{{$config->title}}</td>
                <td>{{\Illuminate\Support\Str::limit($config->value, 150, $end=' ...')}}</td>
				<td style="width:280px;">
                <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
                <a href="{{route('admin.edit.config',$config->id)}}" class="btn btn-warning btn-xs">Editar</a>
				<a href="javascript:;"  onclick="eliminar(this,{{$config->id}});" class="btn btn-danger btn-xs">Eliminar</a>
				</td>
				</tr>
            @endforeach
            @else
            <p class='alert alert-danger'>No hay registros</p>
            @endif

			</table>
			</div>
			</div>
	</div>
</div>
@endsection


@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

function eliminar(a,id) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este ITEM?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
              //  let id_config=$(a).data("id_config");
               let url="{{route('admin.delete.config','-1')}}"
               url=url.replace("-1",id);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"ITEM eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.config")}}';


                      });

                    }else{

                    }
               
                  },
				        error:function(data,message,res){
                    let lista= "<ul>";
                      if (data.responseJSON.errors != 'undefined') {
                        for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                      }
               
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

</script>
@endsection
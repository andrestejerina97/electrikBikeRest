@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
<!--<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-download"></i> Descargar <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
  </ul>
</div>
-->
</div>
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Usuarios</h4>
  </div>
  <div class="card-content table-responsive">
	
	<form method="get" action="{{route('admin.search.user')}}" role="form"  class="form-horizontal">
    @csrf
    <input type="hidden" name="view" value="users">
		<div class="form-group">
			<div class="col-lg-3">
				<input type="tel" name="name" placeholder="Nombre..." class="form-control">
			</div>
			<div class="col-lg-3">
				<input type="text" name="email" placeholder="Nombre usuario..." class="form-control">           				
			</div>
			<div class="col-lg-1">
				
			</div>
			<div class="col-lg-2">
				<button class="btn" >Buscar</button>
			</div>
			<div class="col-lg-2">
            <a href="{{route('admin.edit.user')}}" class="btn btn-primary"><i class='fa fa-male'></i> Nuevo Usuario</a>
			</div>
			
		</div>
	</form>
			
			<table class="table table-bordered table-hover">
			<thead>
			<th>Nombre completo</th>
			<th>Usuario de acceso</th>
			<th>Fecha de alta</th>
			<th></th>
            </thead>
            @if (count($users)>0)
                
		  	@foreach($users as $user)
				
        @if ($user->id==1)
        @else
        <tr>
          <td>{{$user->name}}</td>
          <td>{{$user->email}}</td>
          <td>{{$user->created_at}}</td>
        <td style="width:280px;">
                <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
                <a href="{{route('admin.edit.user',$user->id)}}" class="btn btn-warning btn-xs">Editar</a>
        <a href="javascript:;" data-id_user={{$user->id}} onclick="eliminar(this);" class="btn btn-danger btn-xs">Eliminar</a>
        </td>
        </tr>
        @endif
            @endforeach
            @else
            <p class='alert alert-danger'>No hay registros</p>
            @endif

			</table>
			</div>
			</div>
	</div>
</div>
@endsection


@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este Usuario?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_user=$(a).data("id_user");
               let url="{{route('admin.delete.user','id')}}"
               url=url.replace("id",id_user);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Usuario eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.user")}}';


                      });

                    }else{

                    }
               
                  },
				  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

</script>
@endsection
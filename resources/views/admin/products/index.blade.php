@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
<!--<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-download"></i> Descargar <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
  </ul>
</div>
-->
</div>
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Productos</h4>
  </div>
  <div class="card-content table-responsive">
	
    <form method="get" action="{{route('admin.search.product')}}" role="form"  class="form-horizontal">
      <input type="hidden" name="view" value="products">
		<div class="form-group">
			<div class="col-lg-3">
				<input type="text" name="name" placeholder="Nombre" class="form-control">
			</div>
			<div class="col-lg-3">
        <select id="category_id" name="category_id" class="select2 form-control" style="width: 100%" >
          <option value="">-- SELECCIONE CATEGORÍA--</option>
          @foreach ($categories as $category)
          <option  value="{{$category->id}}"
           @isset($product->category_id)
          @if ($product->category_id == $category->id)
              selected
          @endif
          @endisset
            >{{$category->category_name}} </option>         
          @endforeach
        </select>			</div>
			<div class="col-lg-1">
				
			</div>
			<div class="col-lg-2">
				<button class="btn" >Buscar</button>
			</div>
			<div class="col-lg-2">
            <a href="{{route('admin.edit.product')}}" class="btn btn-primary"><i class='fa fa-male'></i> Nuevo Producto</a>
			</div>
			
		</div>
	</form>
			
			<table class="table table-bordered table-hover">
			<thead>
			<th>Nombre completo</th>
			<th>Código</th>
			<th>Precio oferta</th>
			<th>Stock</th>
			<th></th>
            </thead>
            @if (count($products)>0)
                
			@foreach($products as $product)
				
				<tr>
                <td>{{$product->name}}</td>
                <td>{{$product->code}}</td>
                <td>{{$product->offer_price}}</td>
                <td>{{$product->stock}}</td>
				<td style="width:280px;">
                <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
                <a href="{{route('admin.edit.product',$product->id)}}" class="btn btn-warning btn-xs">Editar</a>
				<a href="javascript:;" data-id_product="{{$product->id}}" onclick="eliminar(this);" class="btn btn-danger btn-xs">Eliminar</a>
				</td>
				</tr>
            @endforeach
            @else
            <p class='alert alert-danger'>No hay productos</p>
            @endif

			</table>
			</div>
			</div>
	</div>
</div>
@endsection


@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este Producto?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_product=$(a).data("id_product");
               let url="{{route('admin.delete.product','id')}}"
               url=url.replace("id",id_product);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Producto eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.product")}}';


                      });

                    }else{

                    }
               
                  },
				  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

</script>
@endsection
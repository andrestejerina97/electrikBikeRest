
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
    <div class="col-md-6">
      <input type="text" name="name" value="@isset($user) {{$user->name}} @endisset" class="form-control" id="name" placeholder="Nombre">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Apellido*</label>
    <div class="col-md-6">
      <input type="text" name="lastname" value="@isset($user){{$user->lastname}}@endisset" required class="form-control" id="lastname" placeholder="Apellido">
    </div>
  </div>
  <div class="form-group" hidden>
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre de usuario*</label>
    <div class="col-md-6">
      <input type="text" hidden name="username"  value="@isset($user){{$user->username}}@endisset" class="form-control" id="username" placeholder="Nombre de usuario">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Email de acceso*</label>
    <div class="col-md-6">
      <input type="email" name="email" value="@isset($user){{$user->email}} @endisset" class="form-control" id="email" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Contrase&ntilde;a</label>
    <div class="col-md-6">
      <input type="password" name="password" class="form-control" id="inputEmail1" placeholder="Contrase&ntilde;a">
<p class="help-block">La contrase&ntilde;a solo se modificara si escribes algo, en caso contrario no se modifica.</p>
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Confirmar Contrase&ntilde;a</label>
    <div class="col-md-6">
      <input type="password" name="password_confirmation" class="form-control" id="inputEmail1" placeholder="Confirmar Contrase&ntilde;a">
<p class="help-block">La contrase&ntilde;a solo se modificara si escribes algo, en caso contrario no se modifica.</p>
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label" ></label>
    <div class="col-md-6 col-lg-6">
    <label>
      Esta activo
      <input type="checkbox" name="is_active" @isset($user)@if($user->is_active)checked @endif @endisset> 
    </label>
  </div>
  </div>


  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label" ></label>
    <div class="col-md-6 col-lg-6">
    <label  >
       Es administrador
      <input type="checkbox" name="is_admin" @isset($user)@if($user->is_admin)checked @endif @endisset> 
    </label>

    </div>
  </div>

  
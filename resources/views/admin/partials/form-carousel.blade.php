
<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
  <div class="col-md-6">
    <input type="text" name="title" value="@isset($carousel) {{$carousel->title}} @endisset" class="form-control" id="title" placeholder="Título">
  </div>
</div>

<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Descripcion*</label>
  <div class="col-md-6">
    <textarea name="description" id="description" class="form-control" placeholder="Descripción larga visible">@isset($carousel) {{$carousel->description}} @endisset</textarea>
  </div>
</div>

<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Tipo*</label>
  <div class="col-md-6">
  
        <select name="type" id="type" class="form-control" required >
          <option value="coverPage"
          @isset($carousel)
          @if($carousel->type=="coverPage")
          selected
          @endif
          @endisset
          selected
          >Portada</option>
        </select>
    
  </div>
</div>

<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Foto :</label>
</div>
  <div class="form-group">
    <div class="col-lg-2"></div>
    <div class="col-lg-10 col-md-6">
      <input type="file" name="photo"  class="form-control"  id="photo" placeholder="Carrusel foto">
    </div>
  </div>



  <div class="form-group text-center show">
    @isset($carousel)
    <label for="inputEmail1" class="col-lg-2 control-label"> Fotos actual</label>

  <div class="col-lg-8">
        <a class="item carouselPhoto"  href="javascript:;" style="color: grey" data-id={{$carousel->id}} data-photo="{{$carousel->photo}}" data-photo_id="{{$carousel->id}}" >
          <img  class="img-responsive"  src="{{asset($carousel->path)}}" alt="">
      </a> 
  </div>
    @endisset
  </div>
 

 



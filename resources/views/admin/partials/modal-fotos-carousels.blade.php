

     <div class="modal " tabindex="-1" id="modal_photo" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="panel">
                   <div>
                       <h4>¿Qué desea hacer?</h4>
                   </div>
                   <div class="input-group">
                    <button type="button" id="btn_modal_delete"   id="submit" class="btn btn-danger  "><i class="fa fa-image"></i> Eliminar foto</button>
                   <!-- <button type="button" id="btn_modal_select_profile" id="submit" class="btn btn-success  "><i class="fa fa-check-circle"></i> Eligir como foto de perfil</button>-->
                </div>            
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
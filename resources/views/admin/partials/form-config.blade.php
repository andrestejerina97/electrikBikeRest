
{{-- <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
    <div class="col-md-6">
    </div>
  </div> --}}
  <input type="hidden"  id="inputTitle" name="title" value="@isset($config) {{$config->title}} @endisset" class="form-control" id="title" placeholder="Título">

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Valor*</label>
    <div class="col-md-6">
      <input name="value" placeholder="Escriba aquí el contenido " value="@isset($config){{$config->value}} @endisset" id="value" class="form-control" >
     <div class="radioValue">
      <div class="form-check ">
        <input class="form-check-input " type="radio" name="radioValue" id="radioValueYes" value="Si" @isset($config)@if($config->value=='Si')checked @endif @endisset>
        <label class="form-check-label " for="exampleRadios1">
          Si
        </label>
      </div>
      <div class="form-check ">
        <input class="form-check-input " type="radio" name="radioValue" id="radioValueNo" value="No" @isset($config)@if($config->value=='No')checked @endif @else checked @endisset>
        <label class="form-check-label " for="exampleRadios2">
          No
        </label>
      </div>
     </div>
      <textarea class="form-control" rows="10" placeholder="Escriba aquí el contenido " name="value" id="textValue" >@isset($config){{$config->value}}@endisset</textarea>
     
    
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Tipo*</label>
    <div class="col-md-6">
    
          <select name="type" id="type" class="form-control" required >
            <option value="SEND_TAX"
            @isset($config)
            @if($config->type=="SEND_TAX")
            selected
            @endif
            @endisset
            >Costo de envío</option>

            <option value="AMOUNT_MINIMAL"
            @isset($config)
            @if($config->type=="AMOUNT_MINIMAL")
            selected
            @endif
            @endisset
            >Monto mínimo</option>
            <option value="AMOUNT_EXTERN"
            @isset($config)
            @if($config->type=="AMOUNT_EXTERN")
            selected
            @endif
            @endisset
            >Monto Fuera de la provincia</option>
            <option value="IG"
            @isset($config)
            @if($config->type=="IG")
            selected
            @endif
            @endisset
            >Instagram</option>
            <option value="WS"
            @isset($config)
            @if($config->type=="WS")
            selected
            @endif
            @endisset
            >Whatsapp</option>
            <option value="FB"
            @isset($config)
            @if($config->type=="FB")
            selected
            @endif
            @endisset
            >Facebook</option>
            <option value="PROVINCE_DEFAULT"
            @isset($config)
            @if($config->type=="PROVINCE_DEFAULT")
            selected
            @endif
            @endisset
            >Provincia por defecto</option>
            <option value="PHONE"
            @isset($config)
            @if($config->type=="PHONE")
            selected
            @endif
            @endisset
            >Teléfono</option>
            <option value="EMAIL"
            @isset($config)
            @if($config->type=="EMAIL")
            selected
            @endif
            @endisset
            >Email de contacto</option>
            <option value="PRIVACITY_POLICY"
            @isset($config)
            @if($config->type=="PRIVACITY_POLICY")
            selected
            @endif
            @endisset
            >Politica de privacidad</option>
            <option value="SHIPPING_POLICY"
            @isset($config)
            @if($config->type=="SHIPPING_POLICY")
            selected
            @endif
            @endisset
            >Politica de envío</option>
            <option value="CONDITIONS"
            @isset($config)
            @if($config->type=="CONDITIONS")
            selected
            @endif
            @endisset
            >Términos y condiciones</option>
            <option value="PICKUPPOINT_DELIVERY"
            @isset($config)
            @if($config->type=="PICKUPPOINT_DELIVERY")
            selected
            @endif
            @endisset
            >Forma de entrega: Delivery</option>
            <option value="PICKUPPOINT_LOCAL"
            @isset($config)
            @if($config->type=="PICKUPPOINT_LOCAL")
            selected
            @endif
            @endisset
            >Forma de entrega: Local</option>
            <option value="METHOD_PAY_MP"
            @isset($config)
            @if($config->type=="METHOD_PAY_MP")
            selected
            @endif
            @endisset
            >Tipo de pago: Mercado Pago</option>
            <option value="METHOD_PAY_TB"
            @isset($config)
            @if($config->type=="METHOD_PAY_TB")
            selected
            @endif
            @endisset
            >Tipo de pago: Transferencia bancaria</option>
            <option value="METHOD_PAY_EF"
            @isset($config)
            @if($config->type=="METHOD_PAY_EF")
            selected
            @endif
            @endisset
            >Tipo de pago: Efectivo</option>
            <option value="MSG_PAY_EF"
            @isset($config)
            @if($config->type=="MSG_PAY_EF")
            selected
            @endif
            @endisset
            >Mensaje para pago: Efectivo</option>
            <option value="MSG_PAY_TB"
            @isset($config)
            @if($config->type=="MSG_PAY_TB")
            selected
            @endif
            @endisset
            >Mensaje para pago: Transferencia bancaria</option>
            <option value="MSG_COMPANY"
            @isset($config)
            @if($config->type=="MSG_COMPANY")
            selected
            @endif
            @endisset
            >Mensaje sección empresa</option>
          </select>
      
    </div>
  </div>
  
   
  
   
  
  
  
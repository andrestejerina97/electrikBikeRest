<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Categoría</label>

  <div class="col-lg-10" >
    <select id="category_id" name="category_id" class="select2 form-control" style="width: 100%" required>
      <option value="">-- SELECCIONE --</option>
      @foreach ($categories as $category)
      <option  value="{{$category->id}}"
       @isset($product->category_id)
      @if ($product->category_id == $category->id)
          selected
      @endif
      @endisset
        >{{$category->category_name}} </option>         
      @endforeach
    </select>
  </div>
</div>
<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Título*</label>
    <div class="col-md-6">
      <input type="text" name="name" value="@isset($product) {{$product->name}} @endisset" class="form-control" id="name" placeholder="Nombre">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Precio oferta*</label>
    <div class="col-md-6">
      <input type="text" name="offer_price" value="@isset($product) {{$product->offer_price}} @endisset" required class="form-control" id="offer_price" placeholder="Precio oferta visible">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Precio</label>
    <div class="col-md-6">
      <input type="text" name="price" value="@isset($product) {{$product->price}} @endisset" required class="form-control" id="price" placeholder="Precio">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Destacado</label>
    <div class="col-md-6">
<label class="checkbox-inline">
</label>
<input type="checkbox" id="inlineCheckbox1" name="destake" required @isset($product) @if($product->destake=="1") checked @endif @endisset value="1"> 

    </div>
  </div>

  
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Descripción </label>
    <div class="col-md-6">
      <input type="text" name="description" value="@isset($product) {{$product->description}} @endisset" class="form-control"  id="description" placeholder="Descripción corta visible">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Descripción Larga*</label>
    <div class="col-md-6">
      <textarea name="description_large" id="description_large" class="form-control" placeholder="Descripción larga visible">@isset($product) {{$product->description_large}} @endisset</textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Casa comercial</label>
    <div class="col-md-6">
      <input type="text" name="comercial_house" value="@isset($product) {{$product->comercial_house}} @endisset" class="form-control" id="comercial_house" placeholder="Marca">
    </div>
  </div>

  
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Código</label>
    <div class="col-md-6">
      <input type="text" name="code" value="@isset($product) {{$product->code}} @endisset" class="form-control" id="code" placeholder="Código">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Stock</label>
    <div class="col-md-6">
      <input type="text" name="stock" value="@isset($product) {{$product->stock}} @endisset" class="form-control" id="stock" placeholder="Stock de producto">
    </div>
  </div>
<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Nuevas fotos :</label>

</div>
  <div class="form-group">
    <div class="col-lg-2"></div>
    <div class="col-lg-10 col-md-6">
      <input type="file" name="photos[]"  class="form-control" multiple id="photos" placeholder="Stock de producto">
    </div>
  </div>



  <div class="form-group text-center show">
    @isset($product)
    <label for="inputEmail1" class="col-lg-2 control-label">{{count($product->images)}} Fotos de producto</label>

  <div class="col-lg-8">
    <div class="owl-carousel owl-theme owl-loaded  productPhotos">
      @foreach ($product->images as $image)
        <a class="item productPhoto"  href="javascript:;" style="color: grey" data-id={{$image->id}} data-photo="{{$image->photo}}" data-photo_id="{{$product->id}}" >
          <img  class="img-responsive"  src="{{asset($image->photo)}}" alt="">
  
          @if($product->avatar==$image->photo)
          FOTO DE PERFIL
          @endif
      </a>
     
      @endforeach
    </div>
  </div>
    @endisset
  </div>
 

 

  

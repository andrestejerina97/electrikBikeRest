

@extends('layouts.admin')
@section('css')
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet"/>

@endsection
@section('content')

<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
<!--<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-download"></i> Descargar <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
  </ul>
</div>
-->
</div>


<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Turnos</h4>
  </div>
  <div class="card-content table-responsive">
<a href="{{route('admin.edit.reservation')}}" class="btn btn-primary">Nuevo Turno</a>

<br><br>
  <form class="form-horizontal" method="get" action="{{route('admin.search.reservation')}}" role="form">
    @csrf

  <div class="form-group">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">Servicio</label>

    </div>
    <div class="col-lg-6">
      <select id="service_id" name="service_id" class="form-control select2" >
        <option value="" selected>-- SELECCIONE --</option>
        @foreach ($services as $service)
        <option data-duration="{{$service->duration}}" data-amount="{{$service->amount}}" value="{{$service->id}}"
         @isset($reservation->service_id)
        @if ($reservation->service_id == $service->id)
            selected
        @endif
        @endisset
          >{{$service->name}} </option>         
        @endforeach
          
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-lg-2">
      <label for="inputEmail1" class="col-lg-2 control-label">Encargado</label>

    </div>
    <div class="col-lg-6">
      <select id="employee_id" name="employee_id" class="form-control select2" >
        <option value="" selected>-- SELECCIONE --</option>
        @foreach ($employees as $employee)
        <option value="{{$employee->id}}"
          >{{$employee->name}} {{$employee->lastname}}</option>         
        @endforeach
      </select>
    </div>
  </div>
  <div class="form-group">
  <div class="col-lg-4">
		<div class="input-group">
		  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		  <input type="date" name="date_at" value="" class="form-control" placeholder="Palabra clave">
		</div>
    </div>
    <div class="col-lg-2">
    	<button class="btn  btn-block" style="margin: 0px 0px 20px 0px">Buscar</button>
    </div>
  </div>
</form>

	
            @if (count($reservations)>0)
			<table class="table table-bordered table-hover">
			<thead>
			<th>Cliente</th>
			<th>Profesional</th>
			<th>Servicio</th>
      <th>Fecha</th>
      <th>Hora Inicio</th>
			<th>Hora Finalización</th>

			<th></th>
			</thead>
            @foreach($reservations as $reservation)
            <tr>
            <td>{{$reservation->pacients->name." ".$reservation->pacients->lastname}}</td>
            <td>{{$reservation->employees->name." ".$reservation->employees->lastname}}</td>
            <td>{{$reservation->services->name}}</td>
            <td>{{$reservation->date_at}}</td>
            <td>{{$reservation->time_at}}</td>
            <td>{{$reservation->time_end}}</td>

            <td style="width:280px;">
            <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
            <a href="{{route('admin.edit.reservation',$reservation->id)}}" class="btn btn-warning btn-xs">Editar</a>
            <a href="javascript:;" onclick="eliminar(this);" data-id_reservation={{$reservation->id}} class="btn btn-danger btn-xs">Eliminar</a>
            </td>
            </tr>
            @endforeach
            @else
            <p class='alert alert-danger'>No hay turnos</p>
            @endif
			</table>
			</div>
			</div>

	</div>
</div>
@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/es.js')}}" type="text/javascript"></script>

<script>
  
    $('.select2').select2({
  language: "es",
});
 
    function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este turno?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_reservation=$(a).data("id_reservation");
               let url="{{route('admin.delete.reservation','id')}}"
               url=url.replace("id",id_reservation);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Turno eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.reservation")}}';


                      });

                    }else{

                    }
               
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }

    </script>    
@endsection



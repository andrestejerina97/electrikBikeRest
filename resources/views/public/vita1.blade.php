
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="PWA Enfócate">
    <meta name="theme-color" content="#ffffff"/>
    <link rel="apple-touch-icon" href="{{asset('images/icons/icon-152x152.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <link rel="stylesheet" href="{{asset('fonts/css/fontawesome-all.min.css')}}">

    <link rel="manifest" href="{{asset('_manifest.json')}}" data-pwa-version="set_by_pwa.js">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@yield("css")
    <script>
    </script>
    <style>
        body, html { overflow-x:hidden; }
      </style>
    <title>Vita</title>
</head>
<body >
    <div id="app">
    <App ></App>
</div>
     <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
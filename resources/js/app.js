
require('./bootstrap');

window.Vue = require('vue');

import App from './components/Vita/App.vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import {routes} from './route.js';

Vue.use(VueRouter);
Vue.prototype.axios = axios;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('App', require('./components/Vita/App.vue').default);
//Vue.component('Navbar', require('./components/Navbar.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 * const app = new Vue({
    el: '#app',
});
 */


const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
})
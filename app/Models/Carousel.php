<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    use HasFactory;
    protected $table="carousels";
    //use HasRoles;
      protected $guarded=['id','created_at','updated_at'];
      protected $hidden = [
        'created_at',
        'updated_at',
        'customer_id',

    ];
    //Scopes
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeTitle($query,$title)
    {
        return $query->where('title',$title);
    }
    public function customers()
    {
        return $this->belongsTo('App\Models\Customer','customer_id', 'id');
    }
    
}

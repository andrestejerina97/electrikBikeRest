<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice_detail extends Model
{
    protected $table="invoice_details";

    protected $guarded=['id','created_at','updated_at'];

}

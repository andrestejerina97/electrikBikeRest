<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $guarded=['id','created_at','updated_at'];

    public function user()
    {
     return $this->belongsTo('App\User','creatur_id','id');
    }
    public function statuses()
    {
     return $this->belongsTo('App\status','status','id');
    }
    public function sending()
    {
     return $this->hasMany('App\sending','invoice_id');
    }
    public function detail()
    {
     return $this->hasMany('App\invoice_detail','invoice_id');
    }
    

}

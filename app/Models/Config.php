<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;
    protected $table="configs";
    //use HasRoles;
      protected $guarded=['id','created_at','updated_at'];
    //Scopes
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeTitle($query,$title)
    {
        return $query->where('title','like','%'.$title.'%');
    }
    public function customers()
    {
        return $this->belongsTo('App\Models\Customer','customer_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
     protected $table="categories";
     protected $guarded=['id','created_at','updated_at'];

    public function Products()
    {
    
        return $this->hasMany('App\Products');
    
    }
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeName($query,$name)
    {
        if($name !=''){
         return $query->where('category_name','LIKE','%'.$name."%");
 
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product_has_sale extends Model
{
    use HasFactory;
    public function sale()
    {
        return $this->belongsTo('App\Models\Sale','sale_id', 'id');
    }
}

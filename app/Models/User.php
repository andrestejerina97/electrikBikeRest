<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use SoftDeletes;
    use HasFactory, Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded=['id','created_at','updated_at'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeName($query,$param)
    {
        if ($param != null) {
            return $query->where('name','LIKE','%'.$param.'%');
        }
    }
    public function scopeEmail($query,$param)
    {
        if ($param != null) {
            return $query->where('email','LIKE','%'.$param.'%');
        }
    }

    public function customers()
    {
        return $this->belongsTo('App\Models\Customer','customer_id', 'id');
    }
   
}

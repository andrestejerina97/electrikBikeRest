<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Customer extends Model
{
  protected $table="customers";
    //use HasRoles;
      protected $guarded=['id','created_at','updated_at'];
}

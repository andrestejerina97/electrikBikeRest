<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
   protected $table='products';
   protected $guarded=['id','created_at','updated_at'];
   public function colors()
   {
    return $this->hasMany('App\Models\Product_has_colors','product_id');
   }
   public function images()
   {
    return $this->hasMany('App\Models\Product_has_images','product_id');
   }
   public function scopeName($query,$name)
   {
       if($name !=''){
        return $query->where('name','LIKE','%'.$name."%");

       }
   }
   public function scopeCategory($query,$category_id)
   {
    if($category_id !=''){
        return $query->where('category_id',$category_id);

       }
   }
   public function scopeCustomer($query,$customer_id)
   {
       return $query->where('customer_id',$customer_id);
   }
}

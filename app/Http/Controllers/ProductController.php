<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\PrductSale;
use App\Models\Product;
use App\Models\Product_has_sale;
use App\Models\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::orderBy('id','asc')->get();
        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products=Product::with('images')->where('id',$id)->orderBy('id','asc')->get();
        return $products;
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destake()
    {
        $products=Product::with('images')->orderBy('id','asc')->get();
        return $products;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

       /**
     * Get products more saled.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function getMoreSaled(Request $request)
    {
        try {

        $customer=Customer::where('id',$request->customer)->firstOrFail();
        $customer_id=$customer->id;

        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            abort(404);
        }
        $sales= DB::table('product_sales')
        ->join('sales','sales.id','product_sales.sale_id')
        ->where('sales.customer_id',$customer_id)
        ->select('product_sales.product_id', DB::raw('count(*) as total'))
        ->groupBy('product_sales.product_id')
        ->get();
        
      return response()->json($sales);
    }
}

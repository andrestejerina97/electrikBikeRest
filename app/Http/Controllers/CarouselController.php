<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Carousel;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CarouselController extends Controller
{
    public function index(Request $request)
    {    try {

      $customer=Customer::where('id',$request->customer)->firstOrFail();
      $customer_id=$customer->id;

      } catch (\Throwable $th) {
          return response()->json(['error' => $th->getMessage()], 400);
      }
      $Carousels=Carousel::Customer($customer_id)->orderBy('id','desc')->orderBy('id','DESC')->get();
     
      $Carousels = ($Carousels) ? $Carousels : array() ;

    return response()->json($Carousels);
    
    }
    public function edit($Carousel_id=0)
    {
    
       
      
     }
     public function search(Request $request)
     {
       
    }
    public function searchCarousel($id,Request $request)
    {
     
    }
    public function update(Request $request)
    {
      $data= $request->except('_token','photo','role','password_confirmation','password');

      if ($request->input('password')=="") {
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('Carousels')->ignore($request->input('id'))],
          'role' => ['required','min:3'],
  
        ]);

      }else{
 
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('Carousels')->ignore($request->input('id'))],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'role' => ['required','min:3'],
  
        ]);
        $data['password']=Hash::make($request->input('password'));
      }

        
          $Carousel=Carousel::where('id','=',$request->input('id'))->update($data);
          return response()->json(['result'=>$request->input('id')]);

    }
 
    public function store(Request $request)
    {

      if($request->has('id')){
        $data= $request->except('_token','photo');

          $validatedData = $request->validate([
            'title' => ['required', 'string', 'max:255'],
          ],
          [
            'title.required' => "El título es requerido",
          ]
        );
        $data= $request->except('_token','photo','id');

        if($request->has('photo')){
            $file=$request->file('photo');
                $path="storage/carousels/".$request->input('id')."/";
                $data['path']=$path."image-carousel-".$request->input('id').".".$file->getClientOriginalExtension();
                $path = $file->storeAs('carousels/'.$request->input('id'),"image-carousel-".$request->input('id').".".$file->getClientOriginalExtension());

            }

       

            $Carousel=Carousel::where('id','=',$request->input('id'))->update($data);

          return response()->json(['result'=>$request->input('id')]);

   
      }else{
        $validatedData = $request->validate([
          'title' => ['required', 'string', 'max:255'],
        ],
        [
          'title.unique' => "El email de acceso ya está en uso,por favor introduzca uno nuevo",
          ]
      );
        $data= $request->except('_token','photo');
        $data['customer_id']=Auth::user()->customer_id;

          $Carousel=Carousel::create($data);
          if($request->has('photo')){
            $file=$request->file('photo');
                $path="storage/carousels/".$Carousel->id."/";
                $Carousel->path=$path."image-carousel-".$Carousel->id.".".$file->getClientOriginalExtension();
                $path = $file->storeAs('carousels/'.$Carousel->id,"image-carousel-".$Carousel->id.".".$file->getClientOriginalExtension());
                $Carousel->save();

            }

        
      }

      return response()->json(['result'=>$Carousel->id]);

    }
    public function delete($id){
      if ($id != 'id') {
        $Carousel= Carousel::find($id);
        $Carousel->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }  
}

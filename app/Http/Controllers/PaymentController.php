<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Invoice_detail;
use App\Models\User;
use Illuminate\Http\Request;
use MP;
use MercadoPago\SDK;
use MercadoPago\Payment;
use MercadoPago\Payer;

class PaymentController extends Controller
{
    
    public function getLinkToPay(Request $request)
    {
        try {

            $customer=Customer::where('id',$request->customer)->firstOrFail();
            $customer_id=$customer->id;
            $user=User::where('customer_id',$customer_id)->firstOrFail();
    
            } catch (\Throwable $th) {
                return response()->json(['error' => $th->getMessage()], 400);
            }


       
         $invoice= new Invoice();
         $invoice->customer_id=$user->id;;
         $invoice->modified_id=$user->id;
         $invoice->customer_id=$customer_id;
         $invoice->creatur_id=$user->id;
         $invoice->total=$request->total;
         $invoice->total_delivery=$request->tax;
         $invoice->cellphone=$request->phone;
         $invoice->observation=$request->email;

        if ($request->has('articulos')) {
            $details=array();
            $invoice->save();

           foreach ($request->articulos as $product) {
               $invoice_details= new Invoice_detail ();
               $invoice_details->quantity= (integer) $product['cantidad'];
               $invoice_details->price=$product['precioFinal'];
               $invoice_details->discount=$product['descuentoMonto'];
               $invoice_details->invoice_id=$invoice->id;
               $invoice_details->save();
               $detail=[
                    'id' => $invoice_details->id,
                    'category_id' => 'Productos',
                    'title' => "Productos tienda eccommerce ".$customer->name,
                    'description' => 'Productos varios ',
                    'picture_url' => '',
                    'quantity' => $invoice_details->quantity,
                    'currency_id' => 'ARS',
                    'unit_price' => (float) $invoice_details->price

               ];
               $details[]=$detail;

           }
        }else{
            return response()->json(['error' => "Detalles no encontrados"], 400);
 
        }
        $items[]=$details;
        $preferenceData = [
            'binary_mode'=>false,
            'items' => $details,
            "back_urls" => array(
                "success" => route('payment.success',['pay'=>'1','invoice'=>$invoice->id]),
                "failure" => route('payment.cancel',['pay'=>'1','invoice'=>$invoice->id]),
                "pending" => route('payment.cancel',['pay'=>'1','invoice'=>$invoice->id]),
            ),
        ];
        MP::setClientId($customer->mp_app_id);
        MP::setClientSecret($customer->mp_app_secret);
        $preference = MP::create_preference($preferenceData);
        //$preference= $preference['response']['sandbox_init_point'];
        //$preference = MP::create_preference($preferenceData);       

       // $preference= $preference['response']['sandbox_init_point'];
        return response()->json(['success'=>true,'payment'=>$invoice->id,'result' => $preference['response']['id']], 200);
    }

    public function successPayment($id,$invoice,Request $request)
    {
        switch ($id) {
            case '1':
                $data=$request->except('site_id');
                $invoiceResult=Invoice::where('id',$invoice)->whereNull('collection_id')->whereNull('collection_status')->update($data);
                if($invoiceResult===1){
                    $invoice=Invoice::find($invoice);
                        $customer=Customer::find($invoice->customer_id);

                        
                        return redirect($customer->description."/"."pagar/?pay=".$invoice->id);
                    }      
                break;
            case '2':
            break;
            default:
            $invoice=Invoice::where('id',$invoice)->first();
            $customer=Customer::find($invoice->customer_id);
            header("Location:".$customer->description."/compra-con-error");

                break;
        }
   

        // $invoice=Invoice::where('id',$invoice)->first();
        // $customer=Customer::find($invoice->customer_id);
        // header("Location:".$customer->description."/compra-con-error");

    }

    public function cancelPayment($id,$invoice,Request $request)
    {
        switch ($id) {
            case '1':
                $data=$request->except('site_id');
                $invoiceResult=Invoice::where('id',$invoice)->whereNull('collection_id')->whereNull('collection_status')->first();
                if($invoiceResult){
                    $invoiceResult->preference_id=$request->preference_id;
                    $customer=Customer::find($invoiceResult->customer_id);
                    $invoiceResult->save();
                }      
                header("Location:".$customer->description."/compra-con-error");

                break;
            case '2':
              
            break;
            default:
            $invoice=Invoice::where('id',$invoice)->first();
            $customer=Customer::find($invoice->customer_id);

            header("Location:".$customer->description."/compra-con-error");
            break;
        }
   

        $invoice=Invoice::where('id',$invoice)->first();
        $customer=Customer::find($invoice->customer_id);

        header("Location:".$customer->description."/compra-con-error");
    }

    public function processPayment(Request $request)
    {

        SDK::setAccessToken("APP_USR-cbd3334a-7f82-4795-86e1-aa9b8cdb9fea");
    
        $payment = new Payment();
        $payment->transaction_amount = (float)$request->transactionAmount;
        $payment->token = $request->token;
        $payment->description = $request->description;
        $payment->installments = (int)$request->installments;
        $payment->payment_method_id = $request->paymentMethodId;
        $payment->issuer_id = (int)$request->issuer;
    
        $payer = new Payer();
        $payer->email = $request->email;
        $payer->identification = array(
            "type" => $request->docType,
            "number" => $request->docNumber
        );
        $payment->payer = $payer;
    
        $payment->save();
    
        $response = array(
            'status' => $payment->status,
            'status_detail' => $payment->status_detail,
            'id' => $payment->id
        );
        return json_encode($response);
    }
}

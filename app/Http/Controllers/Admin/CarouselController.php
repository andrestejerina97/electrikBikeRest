<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Carousel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CarouselController extends Controller
{
    public function index()
    {   
      $customer_id=Auth::user()->customer_id;
        $Carousels=Carousel::Customer($customer_id)->orderBy('id','desc')->orderBy('id','DESC')->get();
        return view('admin.carousels.index')
        ->with('Carousels',$Carousels);
    }
    public function edit($Carousel_id=0)
    {
    
        if ($Carousel_id==0) {
        return view('admin.carousels.edit');  
          
        }else{

        $Carousels=Carousel::where('id','=',$Carousel_id)->get();
      return view('admin.carousels.edit')
      ->with('carousels',$Carousels);
        }
      
     }
     public function search(Request $request)
     {
         $datas=$request->except('_token');
         $customer_id=Auth::user()->customer_id;
         $Carousels= Carousel::Customer($customer_id) 
         ->Title($datas['title'])
         ->orderBy('id','DESC')->get();
       return view('admin.carousels.index',compact('Carousels'));
     
    }
    public function searchCarousel($id,Request $request)
    {
      $Carousels=Carousel::join('Carousels','Carousels.Carousels_id','Carousels.id')
      ->where('publications_id','=',$id)
      ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('Carousels.id','DESC')
      ->with('Carousels')->with('publications')
      ->get();
      return view('admin.carousels.index')
      ->with('Carousels',$Carousels)
      ->with('publications_id',$id);
    }
    public function update(Request $request)
    {
      $data= $request->except('_token','photo','role','password_confirmation','password');

      if ($request->input('password')=="") {
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('Carousels')->ignore($request->input('id'))],
          'role' => ['required','min:3'],
  
        ]);

      }else{
 
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('Carousels')->ignore($request->input('id'))],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'role' => ['required','min:3'],
  
        ]);
        $data['password']=Hash::make($request->input('password'));
      }

        
          $Carousel=Carousel::where('id','=',$request->input('id'))->update($data);
          return response()->json(['result'=>$request->input('id')]);

    }
 
    public function store(Request $request)
    {

      if($request->has('id')){
        $data= $request->except('_token','photo');

          $validatedData = $request->validate([
            'title' => ['required', 'string', 'max:255'],
          ],
          [
            'title.required' => "El título es requerido",
          ]
        );
        $data= $request->except('_token','photo','id');

        if($request->has('photo')){
            $file=$request->file('photo');
                $path="storage/carousels/".$request->input('id')."/";
                $data['path']=$path."image-carousel-".$request->input('id').".".$file->getClientOriginalExtension();
                $path = $file->storeAs('carousels/'.$request->input('id'),"image-carousel-".$request->input('id').".".$file->getClientOriginalExtension());

            }

       

            $Carousel=Carousel::where('id','=',$request->input('id'))->update($data);

          return response()->json(['result'=>$request->input('id')]);

   
      }else{
        $validatedData = $request->validate([
          'title' => ['required', 'string', 'max:255'],
        ],
        [
          'title.unique' => "El email de acceso ya está en uso,por favor introduzca uno nuevo",
          ]
      );
        $data= $request->except('_token','photo');
        $data['customer_id']=Auth::user()->customer_id;

          $Carousel=Carousel::create($data);
          if($request->has('photo')){
            $file=$request->file('photo');
                $path="storage/carousels/".$Carousel->id."/";
                $Carousel->path=$path."image-carousel-".$Carousel->id.".".$file->getClientOriginalExtension();
                $path = $file->storeAs('carousels/'.$Carousel->id,"image-carousel-".$Carousel->id.".".$file->getClientOriginalExtension());
                $Carousel->save();

            }

        
      }

      return response()->json(['result'=>$Carousel->id]);

    }
    public function delete($id){
      if ($id != 'id') {
        $Carousel= Carousel::find($id);
        $Carousel->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }  
}

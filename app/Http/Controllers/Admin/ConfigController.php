<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ConfigController extends Controller
{
    public function index()
    {   
      $customer_id=Auth::user()->customer_id;
        $Configs=Config::Customer($customer_id)->orderBy('id','desc')->orderBy('id','DESC')->get();
        return view('admin.config.index')
        ->with('configs',$Configs);
    }
    public function edit($Config_id=0)
    {
    
        if ($Config_id==0) {
        return view('admin.config.edit');  
          
        }else{

        $Configs=Config::where('id','=',$Config_id)->get();
      return view('admin.config.edit')
      ->with('configs',$Configs);
        }
      
     }
     public function search(Request $request)
     {
         $datas=$request->except('_token');
         $customer_id=Auth::user()->customer_id;
         $configs= Config::Customer($customer_id) 
         ->Title($datas['title'])
         ->orderBy('id','DESC')->get();
       return view('admin.config.index',compact('configs'));
     
    }
    public function searchConfig($id,Request $request)
    {
      $Configs=Config::join('config','Configs.Configs_id','Configs.id')
      ->where('publications_id','=',$id)
      ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('Configs.id','DESC')
      ->with('Configs')->with('publications')
      ->get();
      return view('admin.config.index')
      ->with('Configs',$Configs)
      ->with('publications_id',$id);
    }
    public function update(Request $request)
    {
      $data= $request->except('_token','photo','role','password_confirmation','password');

      if ($request->input('password')=="") {
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('Configs')->ignore($request->input('id'))],
          'role' => ['required','min:3'],
  
        ]);

      }else{
 
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('Configs')->ignore($request->input('id'))],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'role' => ['required','min:3'],
  
        ]);
        $data['password']=Hash::make($request->input('password'));
      }

        
          $Config=Config::where('id','=',$request->input('id'))->update($data);
          return response()->json(['result'=>$request->input('id')]);

    }
 
    public function store(Request $request)
    {

      if($request->has('id')){
        $data= $request->except('_token','radioValue');
        $data['customer_id']=Auth::user()->customer_id;

        $validatedData = $request->validate([
          'title' => ['required', 'string', 'max:255'],
          'type' => ['required',Rule::unique('configs')->where('type', $data['type'])
            ->where('customer_id',$data['customer_id'])
            ->ignore($data['id'])
        ],
        ],
        [
          'title.unique' => "El título de acceso es requerido",
          'type.unique' => "Este parámetro ya fue definido",

          ]
      );
     


          $Config=Config::where('id','=',$request->input('id'))->update($data);

          return response()->json(['result'=>$request->input('id')]);

   
      }else{
        $data= $request->except('_token');
        $data['customer_id']=Auth::user()->customer_id;
        $validatedData = $request->validate([
          'title' => ['required', 'string', 'max:255'],
          'type' => 'required|unique:configs,type,NULL,id,customer_id,'.$data['customer_id'],
        ],
        [
          'title.unique' => "El título de acceso es requerido",
          'type.unique' => "Este parámetro ya fue definido",

          ]
      );
      

          $Config=Config::create($data);
         

        
      }

      return response()->json(['result'=>$Config->id]);

    }
    public function delete($id){
      if ($id != 'id') {
        $Config= Config::find($id);
        $Config->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }  
}

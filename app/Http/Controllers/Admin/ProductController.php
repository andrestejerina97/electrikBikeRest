<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_has_images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index()
    {
        $customerId=Auth::user()->customer_id;
        $products=Product::Customer($customerId)->with('images')->orderBy('id','desc')->get();
        $categories=Category::where('customer_id',Auth::user()->customer_id)->orderBy('category_name','DESC')->get();

        return view('admin.products.index',compact('products','categories'));
    }
    public function edit($Product_id=0)
    {
    
        if ($Product_id==0) {
          $categories=Category::where('customer_id',Auth::user()->customer_id)->orderBy('category_name','DESC')->get();

        return view('admin.products.edit')  
                ->with('categories',$categories);

        }else{

        $products=Product::with('images')->where('id','=',$Product_id)->get();
        $categories=Category::where('customer_id',Auth::user()->customer_id)->orderBy('category_name','DESC')->get();
      return view('admin.products.edit')
      ->with('products',$products)
      ->with('categories',$categories);

        }
      
     }
    
     public function store(Request $request)
     {
         $data=$request->except('_token','id','photos');
         $data['customer_id']=Auth::user()->customer_id;
         $customerId=$data['customer_id'];

         if($request->has('id')){
          /*  $validatedData = $request->validate([
                'email' => ['required', Rule::unique('Products')->ignore($request->input('id'))]
              ]);*/
              if($request->has('photos')){
                $files=$request->file('photos');
                $totalCount=Product_has_images::where('product_id',$request->id)->get()->count() + 1;
                foreach ($files as $file) {
                    $image=new Product_has_images ();
                    $path="storage/products/".$request->id."/";
                    $image->photo=$path."image".$totalCount.".".$file->getClientOriginalExtension();
                    $image->product_id=$request->id;
                    $image->customer_id=$customerId;

                    $path = $file->storeAs('products/'.$request->id,"image".$totalCount.".".$file->getClientOriginalExtension());
                    $image->save();
                    $totalCount++;
                }
    
                }

                if (!is_numeric($request->category_id)) {
                  $category= Category::create([
                    'category_name'=>$request->category_id,
                    'customer_id'=>$data['customer_id'],
                  ]);
                  $data['category_id']=$category->id;
                }else{
                  //check security
                  $category=Category::find($request->category_id);
                  $data['category_id']=$category->id;
      
                }
         $Product=Product::where("id",'=',$request->input('id'))->update($data);
  
         return response()->json(['result'=>$request->input('id')]);
        }else{
          /*  $validatedData = $request->validate([
                'email' => ['required','unique:Products'],
              ]);*/


          if (!is_numeric($request->category_id)) {
            $category= Category::create([
              'category_name'=>$request->category_id,
              'customer_id'=>$data['customer_id'],
            ]);
            $data['category_id']=$category->id;
          }else{
            //check security
            $category=Category::find($request->category_id);
            $data['category_id']=$category->id;

          }
          $Product=Product::create($data);

          if($request->has('photos')){
            $files=$request->file('photos');
            $totalCount=Product_has_images::where('product_id',$request->id)->get()->count() + 1;
            foreach ($files as $file) {
                $image=new Product_has_images ();
                $path="storage/products/".$request->id."/";
                $image->photo=$path."image".$totalCount.".".$file->getClientOriginalExtension();
                $image->product_id=$Product->id;
                $image->customer_id=$customerId;

                $path = $file->storeAs('products/'.$Product->id,"image".$totalCount.".".$file->getClientOriginalExtension());
                $image->save();
                $totalCount++;
            }

            }

          return response()->json(['result'=>$Product->id]); 
               
      }
     }
     public function search(Request $request)
     {
         $datas=$request->except('_token');
         $products= Product::Customer(Auth::user()->customer_id) 
         ->category($datas['category_id'])
         ->Name($datas['name'])
         ->orderBy('id','DESC')->get();
         $categories=Category::where('customer_id',Auth::user()->customer_id)->orderBy('category_name','DESC')->get();

       return view('admin.products.index',compact('products','categories'));
     
    }

     public function delete($id){
        if ($id != 'id') {
          $user= Product::find($id);
          $user->delete();
          return response()->json(['result'=>1]);
        }else{
          return response()->json(['result'=>-1]);

        }
      }
      public function activePhoto($id=0,Request $request)
      {
          if ($id!= 0) {
            $Product=Products::find($id);
            $Product->avatar=$request->input("photo");
            $Product->save();
            return response()->json(['result'=>$Product->save()]);
    
          }
      }
      public function deletePhoto($id=0,Request $request)
      {
          if ($id!= 0) {
            $photo=$request->input("photo");
            //$Product_images=Product_images::find($id);
            $Product=Product::find($id);
            if ($Product->avatar==$photo) {
              $Product->avatar="";
              $Product->save();
            }
            if ($request->has("id_photo")) {
              $Product_has_images=Product_has_images::where("id",$request->input("id_photo"))->first();
              $pathPhoto=$Product_has_images->photo;

             // $path = Storage::delete();
             $path=\Storage::delete($pathPhoto);
             $Product_has_images->delete();
            }
  
            return response()->json(['result'=>1]);
    
          }else{
            return response()->json(['result'=>-1]);
          }
      }
}

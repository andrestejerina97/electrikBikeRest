<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    public function index()
    {
        $customerId=Auth::user()->customer_id;
        $categories=Category::Customer($customerId)->orderBy('id','desc')->get();
        return view('admin.categories.index',compact('categories'));
    }
    public function edit($Category_id=0)
    {
    
        if ($Category_id==0) {
        return view('admin.categories.edit');  
          
        }else{

        $categories=Category::where('id','=',$Category_id)->get();
      return view('admin.categories.edit')
      ->with('categories',$categories);
        }
      
     }
    
     public function store(Request $request)
     {
         $data=$request->except('_token','id','photos');
         if($request->has('id')){
          /*  $validatedData = $request->validate([
                'email' => ['required', Rule::unique('categories')->ignore($request->input('id'))]
              ]);*/
           
         $Category=Category::where("id",'=',$request->input('id'))->update($data);
  
         return response()->json(['result'=>$request->input('id')]);
        }else{
          /*  $validatedData = $request->validate([
                'email' => ['required','unique:categories'],
              ]);*/
          $data['customer_id']=Auth::user()->customer_id;
          
          $Category=Category::create($data);
          return response()->json(['result'=>$Category->id]); 
               
      }
     }
     public function search(Request $request)
     {
         $datas=$request->except('_token');
         $categories= Category::Customer(Auth::user()->customer_id) 
         ->Name($datas['category_name'])
         ->orderBy('id','DESC')->get();
       return view('admin.categories.index',compact('categories'));
     
    }

     public function delete($id){
        if ($id != 'id') {
          $user= Category::find($id);
          $user->delete();
          return response()->json(['result'=>1]);
        }else{
          return response()->json(['result'=>-1]);

        }
      }
}

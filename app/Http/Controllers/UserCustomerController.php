<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;
use App\Mail\WholeSale;
use App\Models\Customer;
use App\Models\UserCustomer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserCustomerController extends Controller
{
      /**
     * Show the userCustomers
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        try {

            $customer_id=$request->customer;
        } catch (\Throwable $th) {
            abort(404);
        }
        $users= UserCustomer::where('customer_id',$customer_id)->first();
        $users = ($users) ? $users : array() ;
        return response()->json($users) ;
    }
        /**
     * create the userCustomers
     *
     * @return 
     */
    public function create(Request $request)
    {
        $validate=$request->validate([
            'email'=>'required',
            'phone'=>'required',
            'name'=>'required',
        ]);
        $users= new UserCustomer();
        $users->email=$request->input('email','');
        $users->name=$request->input('name','');
        $users->distric=$request->input('distric','');
        $users->street=$request->input('street','');
        $users->city=$request->input('city','');
        $users->codePostal=$request->input('codePostal','');
        $users->addresse=$request->input('addresse','');
        $users->addresse2=$request->input('addresseReciver','');
        $users->phone=$request->input('phone','');
        $users->dni=$request->input('dni','');
        $users->token=1;
        $users->customer_id=$request->input('customer_id',0);

        $users->save();
        $users->token=sha1($users->id);
        $users->save();
        $users->id=null;
        $users->customer_id=null;

        return response()->json($users);
    }
    public function sendEmailWholesale(Request $request)
    {
        try {

            $customer_id=$request->customer;
            $customer=Customer::findOrFail($customer_id);
            $data=$request->except('_token','customer');
            $configuration = [
                'smtp_host'    => $customer->smtp_host,
                'smtp_port'    => $customer->smtp_port,
                'smtp_username'  => $customer->smtp_username,
                'smtp_password'  => $customer->smtp_password,
                'smtp_encryption'  => $customer->smtp_encriptation,
                
                'from_email'    => $customer->smtp_from,
                'from_name'    => $customer->name,
               ];
                
            $mailer = app()->makeWith('user.mailer', $configuration);

           // Mail::setSwiftMailer(new \Swift_Mailer($transport));
            // Config::set('mail.host',$customer->smtp_host);
            // Config::set('mail.port',$customer->smtp_port);
            // Config::set('mail.username',$customer->smtp_username);
            // Config::set('mail.password',$customer->smtp_password);
            // Config::set('mail.encryption',$customer->smtp_encriptation);
            // Config::set('mail.from',array('address' => $customer->smtp_from, 'name' =>  $customer->name));
            $mailer = app()->makeWith('user.mailer', $configuration);
         
            $mailer->to((string)$customer->smtp_from)//$customer->replyTo
            ->bcc('tejelibre@gmail.com')
            ->send(new WholeSale($data,"NUEVA CONSULTA POR VENTA MAYORISTA",array('address' => $customer->smtp_from, 'name' =>  $customer->name)));


        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $th->getMessage();
            abort(404);
        }

        return response()->json(array('result'=>true));

    }
    public function sendContact(Request $request)
    {
        try {

            $customer_id=$request->customer;
            $customer=Customer::findOrFail($customer_id);
            $data=$request->except('_token','customer');
            $data['bag_type']=implode(',',$data['bag_type']);
            $data['print_type']=implode(',',$data['print_type']);
            $data['handles_type']=implode(',',$data['handles_type']);
            $configuration = [
                'smtp_host'    => $customer->smtp_host,
                'smtp_port'    => $customer->smtp_port,
                'smtp_username'  => $customer->smtp_username,
                'smtp_password'  => $customer->smtp_password,
                'smtp_encryption'  => $customer->smtp_encriptation,
                
                'from_email'    => $customer->smtp_from,
                'from_name'    => $customer->name,
               ];
                
            $mailer = app()->makeWith('user.mailer', $configuration);
         
            $mailer->to((string)$customer->smtp_from)//$customer->replyTo
            ->bcc('tejelibre@gmail.com')
            ->send(new ContactForm($data,"NUEVA CONSULTA GENERAL",array('address' => $customer->smtp_from, 'name' =>  $customer->name)));


        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return $th->getMessage();
            abort(404);
        }

        return response()->json(array('result'=>true));

    }
}

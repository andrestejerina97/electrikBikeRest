<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Customer;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
        /**
     * Show the SEND TAX.
     *
     * @return  Illuminate\Http\Request\json;
     */
    public function getSendTax(Request $request)
    {
        try {

            $customer=Customer::where('id',$request->customer)->firstOrFail();
            $customer_id=$customer->id;
    
            } catch (\Throwable $th) {
                return response()->json(['error' => $th->getMessage()], 400);
            }
            $configs= Config::where('customer_id',$customer_id)->get();
            $configs = ($configs) ? $configs : array() ;

          return response()->json($configs);
    }

}

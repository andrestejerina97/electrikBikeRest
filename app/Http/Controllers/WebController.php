<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebController extends Controller
{
    public function index($token,Request $request)
    {
        try {
            $customer= Customer::where('keyname',$token)->first();
        } catch (\Throwable $th) {
            dd($th->getMessage());
            abort(404);
        }
        return view("public.".$customer->keyname);
    }

    public function province(Request $request)
    {
      //  $provinces=DB::table('POR')
       // return view("public.".$customer->keyname);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WholeSale extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    protected $data;
    protected $fromTo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$message,$from)
    {
        $this->message=$message;
        $this->data=$data;
        $this->fromTo=$from;

    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->from)
        ->subject("Venta mayorista")
        ->markdown('emails.WholeSaleTemplate',
        [
            'data' => $this->data,
            'message' => $this->message,

        ]);
    }
}

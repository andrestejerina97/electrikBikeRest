<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    protected $data;
    protected $fromTo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$message,$from)
    {
        $this->message=$message;
        $this->data=$data;
        $this->fromTo=$from;

    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->fromTo['address'],$this->fromTo['name'])
        ->subject("Formulario de consultas")
        ->markdown('emails.ContactTemplate',
        [
            'data' => $this->data,
            'message' => $this->message,

        ]);
    }
}
